import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createMuiTheme, MuiThemeProvider, responsiveFontSizes} from "@material-ui/core";

let theme = createMuiTheme({

    // palette: {
    //
    //     background: {
    //         paper: 'rgba(201,189,151,0.93)',
    //         // backgroundImage: `url(${imageUrl})`,
    //         // background: ("/background.jpg"),
    //         default: '#ede1b5'
    //     },
    //     primary: {
    //         main: '#394832',
    //     },
    //     secondary: {
    //         main: '#ea7e24',
    //     },
    // },
    // typography: {
    //     fontFamily: [
    //         'arial',
    //     ].join(','),
    // },

});

theme = responsiveFontSizes(theme);

const APP = () =>
    <MuiThemeProvider theme={theme}>
            <App/>
    </MuiThemeProvider>

ReactDOM.render(<APP/>, document.getElementById('root'));

