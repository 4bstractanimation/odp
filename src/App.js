import React from 'react';
import Route from "./routes";
import {BrowserRouter} from "react-router-dom";
import {FireBase} from "./components/firebase";
import { SnackbarProvider } from 'notistack';
import styles from "./tableResponsive.css"



function App() {
    FireBase();
    return (
        <SnackbarProvider SnackbarProps={{ autoHideDuration: 4000 }}>
            <BrowserRouter>
                <Route/>
            </BrowserRouter>
        </SnackbarProvider>
    );
}

export default App;
