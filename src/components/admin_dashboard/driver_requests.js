import React, { forwardRef, Fragment, useEffect, useState} from 'react';
import MaterialTable, {MTableCell, MTableEditField} from 'material-table';
import {
    AddBox, ArrowUpward,
    Check, ChevronLeft,
    ChevronRight,
    Clear,
    DeleteOutline,
    Edit,
    FilterList,
    FirstPage, LastPage, Remove,
    SaveAlt, Search, ViewColumn
} from "@material-ui/icons";
import {useList} from "react-firebase-hooks/database";
import app from "firebase"
import AcceptRejectDialog from "./accept_reject_dialog"


function ViewExisting(){

    const [id,setID] = useState();
    const [open,setOpen] = useState(false);

    const [data,setData] = useState();

    const Reference = app.database().ref("Drivers");
    const [value_data, loading_data, error_data] = useList(Reference);






    useEffect( () => {
        if(!loading_data && !error_data){

            setData(JSON.parse(JSON.stringify(value_data)));
        }
        }, [loading_data, value_data])      ;

    const handleRowClick = (e,row) =>{
        setID(row.id);
        setOpen(true);
    }

    const columns = [
        { title: 'Name', field: 'Name' },
        { title: 'Cnic', field: 'Cnic' },
        { title: 'License number ', field: 'License number' },
        { title: 'Contact number ', field: 'Contact number' },
        { title: 'Status', field: 'Status'},
    ];


    const tableIcons = {
            Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
            Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
            Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
            Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
            DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
            Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
            Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
            FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
            LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
            NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
            ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
            Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
            SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
            ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
            ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

        return(
            <Fragment>
                <MaterialTable
                    icons={tableIcons}
                    title="Driver Requests"
                    columns={columns}
                    data={data}
                    isLoading={loading_data}
                    onRowClick={handleRowClick}
                    components={{
                        Cell: props => (
                            <MTableCell data-column={props.columnDef.title} {...props} />
                        ),
                        EditField: props => (

                            <MTableEditField variant={"outlined"} label={props.columnDef.title}  {...props} />
                        ),

                    }}

                    options={{
                        columnsButton:true,
                        exportButton:true,
                    }}
                />

                {open?<AcceptRejectDialog close={setOpen} Reference={Reference} id={id} />:null}

            </Fragment>
            )}

export default ViewExisting;

