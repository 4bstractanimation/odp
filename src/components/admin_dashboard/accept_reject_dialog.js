import React, {useState} from "react";
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    DialogContentText,
    Button,
    TextField
} from "@material-ui/core";
import {useSnackbar} from "notistack";

export default (props)=> {

    const { enqueueSnackbar }  = useSnackbar();

     const Success = (message) => {
        enqueueSnackbar(
          message, {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };

    const handleClick = (Status)=> {
        if(props.Requests){
            props.Reference.child(props.id).update({"Status": Status,"Price":price}).then(()=>Success("Package request status changed!"));
            props.close(false)
        }else {
            props.Reference.child(props.id).update({"Status": Status}).then(()=>Success("Driver request status changed!"));;
            props.close(false)
        }
    };
    const [price,setPrice] = useState("NA");

    return <Dialog
                open={true}
                onClose={()=>props.close(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">  {props.Requests?"Package Request":"Driver Request"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {props.Requests?
                            <TextField
                                onChange={(e)=>setPrice(e.target.value)}
                                label={"Price"}
                                variant={"outlined"}
                                type={"number"}
                            />
                            :"Do you want to accept this driver?"
                        }

                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=>handleClick("accepted")} color="primary" autoFocus>
                        Accept
                    </Button>
                    <Button onClick={()=>handleClick("rejected")} color="primary" >
                        Reject
                    </Button>
                </DialogActions>
            </Dialog>
}