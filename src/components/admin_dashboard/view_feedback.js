import React, {Component, forwardRef, useEffect, useState} from 'react';
import MaterialTable, {MTableCell, MTableEditField} from 'material-table';
import {
    AddBox, ArrowUpward,
    Check, ChevronLeft,
    ChevronRight,
    Clear,
    DeleteOutline,
    Edit,
    FilterList,
    FirstPage, LastPage, Remove,
    SaveAlt, Search, ViewColumn
} from "@material-ui/icons";
import {useList} from "react-firebase-hooks/database";
import app from "firebase"
import {useSnackbar} from "notistack";
import {TextField} from "@material-ui/core";


function ViewExisting(){

    const { enqueueSnackbar }  = useSnackbar();

    const [data,setData] = useState();

    const Reference = app.database().ref("Feedback");
    const [value_data, loading_data, error_data] = useList(Reference);

     const Success = (message) => {
        enqueueSnackbar(
          message, {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };



    useEffect( () => {
        if(!loading_data && !error_data){

            setData(JSON.parse(JSON.stringify(value_data)));
        }
        }, [loading_data, value_data]);

    const columns = [
        { title: 'Comments', field: 'Comment' , render: (row)=><p  style={{width:"100%", wordBreak:"break-all"}}>{row.Comment}</p>},
    ];




    const tableIcons = {
            Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
            Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
            Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
            Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
            DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
            Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
            Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
            FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
            LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
            NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
            ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
            Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
            SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
            ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
            ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

        return(

            <MaterialTable
                icons={tableIcons}
                title="Feedback"
                columns={columns}
                data={data}
                isLoading={loading_data}
                editable={{
                    onRowDelete: oldData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                Reference.child(oldData.id).remove().then(()=>{
                                    Success("Data deleted successfully!")
                                })
                                }, 600);
                        }),
                }}
                components={{
                        Cell: props => (
                            <MTableCell data-column={props.columnDef.title} {...props} />
                        ),
                        EditField: props => (

                            <MTableEditField variant={"outlined"} label={props.columnDef.title}  {...props} />
                        ),

                    }}

                    options={{
                        columnsButton:true,
                        exportButton:true,
                    }}
            />

            )}

export default ViewExisting;
