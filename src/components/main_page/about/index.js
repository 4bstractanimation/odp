import React, {Fragment} from "react";
import {Paper, Typography, Grid, makeStyles} from "@material-ui/core";
import HoverPaper from "../hoverPaper"

const useStyles = makeStyles(theme => ({
    paper: {
        padding: 50,
    },
}));


export default () => {
    const classes = useStyles();
   return <div id={"about"} style={{textAlign: "center"}}>
        <Typography align={"center"} variant={"h4"}>About us</Typography>
        <Grid container justify={"center"} style={{marginTop: 50}} >
            <Grid xl={6} lg={7} md={8} sm={10} xs={12} >
                <HoverPaper className={classes.paper} >
                    <Typography variant={"subtitle1"} >
                        Most of the people have to face the problem of the pick and drop service of their
                        school and college going children and teachers. The pick and drop services to the office working men and women are also provided.
                        They look for the perfect and most reliable pick and drop service. we has most reliable, regular, punctual and responsible drivers
                        with good condition vehicles to provide efficient pick and drop services to the students in Islamabad and Rawalpindi. The parents
                        can rely on our service as we provide efficient and professional pick and drop service providers. They should feel free to contact
                        with us for the provision of pick and drop services from their homes to the schools, colleges, offices and other place of work.
                        We charge from the parents, men and women at the most appropriate and affordable charges from them for the provision of pick and drop service.
                    </Typography>
                </HoverPaper>
            </Grid>
        </Grid>
    </div>
}