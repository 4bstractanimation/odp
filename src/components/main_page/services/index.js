import React, {Fragment} from "react";
import {makeStyles, Typography, useMediaQuery, useTheme} from "@material-ui/core";
import {School} from "@material-ui/icons"
import HoverPaper from "../hoverPaper"
import Grid from "@material-ui/core/Grid";
import {LocalTaxi,SentimentVerySatisfied,ThumbUp} from '@material-ui/icons';





const slides = [
    { element : <Fragment>
                    <LocalTaxi color={"secondary"} style={{fontSize: 150}}/>
                     <Typography align={"center"} variant={"h4"}> Pick and Drop Service </Typography>
                </Fragment>
    },
    { element : <Fragment>
                    <SentimentVerySatisfied color={"secondary"} style={{fontSize: 150}}/>
                     <Typography align={"center"} variant={"h4"}> Customer Satisfaction </Typography>
                </Fragment>
    },
    { element : <Fragment>
                    <ThumbUp color={"secondary"} style={{fontSize: 150}}/>
                     <Typography align={"center"} variant={"h4"}> Quality Assurance </Typography>
                </Fragment>
    },

]



const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(10),
  },

}));


export default function () {
    const classes= useStyles();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('sm'));

    return <div id={"services"} style={{textAlign: "center"}}>
        <Typography align={"center"} variant={"h4"}>Our Services</Typography>

        <Grid container justify={"center"} style={{marginTop:50}}>
            <Grid item xl={9} lg={10} md={11} sm={12} xs={12} >
                <Grid container justify={"center"} spacing={matches?0:10} >
                    {slides.map((item) =>
                        <Grid item  xl={4} lg={4} md={5} sm={7} xs={12} style={{marginBottom:matches?50:0}} >
                            <HoverPaper  className={classes.paper} >
                                {item.element}
                            </HoverPaper>
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Grid>

    </div>
}