import React from "react";
import {Typography} from "@material-ui/core";
import HoverPaper from "../hoverPaper"
import Button from "@material-ui/core/Button";
import {A} from "hookrouter";
// eslint-disable-next-line no-unused-vars
import packageStyle from "./package.css"


export default ()=>{
    return(
        <div id="price">
            <Typography variant={"h4"} style={{marginBottom:40}}>Packages</Typography>

            <HoverPaper className="plan">
                <div className="plan-inner">
                    <div className="entry-title">
                        <h3>Weekly Plan</h3>
                        <div className="price">9Rs<span>/PER KM</span>
                        </div>
                    </div>
                    <div className="entry-content">
                        <ul>
                            <li><strong>1x</strong> option 1</li>
                            <li><strong>2x</strong> option 2</li>
                            <li><strong>3x</strong> option 3</li>
                            <li><strong>Free</strong> option 4</li>
                            <li><strong>Unlimited</strong> option 5</li>
                        </ul>
                    </div>

                      <div style={{padding:"3em 0"}}>
                        <Button component={A}  color={"secondary"} variant={"outlined"} href="/Login">Order Now</Button>
                    </div>

                </div>
            </HoverPaper>


            <HoverPaper className="plan basic">
                <div className="plan-inner">
                    <div className="hot">hot</div>
                    <div className="entry-title">
                        <h3>Monthly Plan</h3>
                        <div className="price">7Rs<span>/PER KM</span>
                        </div>
                    </div>
                    <div className="entry-content">
                        <ul>
                            <li><strong>1x</strong> option 1</li>
                            <li><strong>2x</strong> option 2</li>
                            <li><strong>3x</strong> option 3</li>
                            <li><strong>Free</strong> option 4</li>
                            <li><strong>Unlimited</strong> option 5</li>
                        </ul>
                    </div>
                      <div style={{padding:"3em 0"}}>
                        <Button component={A}  color={"secondary"} variant={"outlined"} href="/Login">Order Now</Button>
                    </div>
                </div>
            </HoverPaper>

            <HoverPaper className="plan standard">
                <div className="plan-inner">
                    <div className="entry-title">
                        <h3>Yearly Plan</h3>
                        <div className="price">5Rs<span>/PER KM</span>
                        </div>
                    </div>
                    <div className="entry-content">
                        <ul>
                            <li><strong>2x</strong> Free Entrance</li>
                            <li><strong>Free</strong> Snacks</li>
                            <li><strong>Custom</strong> Swags</li>
                            <li><strong>2x</strong> Certificate</li>
                            <li><strong>Free</strong> Wifi</li>
                        </ul>
                    </div>
                  <div style={{padding:"3em 0"}}>
                        <Button component={A}  color={"secondary"} variant={"outlined"} href="/Login">Order Now</Button>
                    </div>
                </div>
            </HoverPaper>

        </div>
    )
}