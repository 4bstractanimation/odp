import React, {Fragment} from "react";
import {Paper, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {makeStyles, useTheme} from "@material-ui/core/styles";


function Footer() {

    const theme = useTheme();
    const useStyles = makeStyles(theme => ({
        footerContainer: {
            height: "fit-content",
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.primary.contrastText
        },
        subFooter: {
            height: "fit-content",
            padding: 20,
            textAlign: "center",
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.primary.contrastText
        }
    }));
    const classes = useStyles();
    return (
        <Fragment>
            <Paper elevation={4} className={classes.footerContainer}>
                <Grid container justify={"center"} style={{}}>
                    ​
                    <Grid md={4} xs={12} style={{padding: 50,}}>
                        <Typography variant={"h5"}>ODP</Typography>
                        <Typography variant={"body2"}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                            dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                        </Typography>
                    </Grid>
                    <Grid md={4} xs={12} style={{padding: 50,}}>
                        <Typography variant={"h5"}>Useful Links</Typography>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 1</Button>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 2</Button>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 3</Button>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 4</Button>
                        ​
                        ​
                    </Grid>
                    <Grid md={4} xs={12} style={{padding: 50,}}>
                        <Typography variant={"h5"}>Contact us</Typography>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 1</Button>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 2</Button>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 3</Button>
                        ​
                        ​
                        <Button color={"inherit"} style={{display: "block"}}>Link 4</Button>
                        ​
                        ​
                    </Grid>
                    ​
                </Grid>
            </Paper>
            <Paper elevation={4} className={classes.subFooter}>
                &copy; {new Date().getFullYear()} Copyright: <Button color={"inherit"} href="#"> ODP </Button>
            </Paper>
        </Fragment>
    );
}

export default Footer