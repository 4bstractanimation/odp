import React, {Fragment} from "react";
import Events from "../group_slider"
import {Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";


const slides = [
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    },
    {
        element: <div style={{display: "table-caption"}}>
            <img
                src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2bhlEyOoQ8PxxtHdT_yCq9N4NAuJt-KO-K2nMjz5kOamV-yY"}
                style={{width: 400,}}/>
            <Button variant={"outlined"} color={"secondary"}> View Details </Button>
        </div>
    }
]


export default () => <div id={"events"} style={{textAlign: "center"}}>
    <Typography align={"center"} variant={"h4"}>Upcoming Events</Typography>
    <Events padding={1} slides={slides}/>

</div>