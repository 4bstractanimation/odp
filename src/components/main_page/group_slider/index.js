import React, {useState} from "react";
import {makeStyles, Paper, Typography} from "@material-ui/core";
import 'react-animated-slider/build/horizontal.css';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import HoverPaper from "../hoverPaper"
const responsive = {
    desktop: {
        breakpoint: {max: 3000, min: 1024},
        items: 3,
        slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
        breakpoint: {max: 1024, min: 704},
        items: 2,
        slidesToSlide: 2, // optional, default to 1.
    },
    mobile: {
        breakpoint: {max: 704, min: 0},
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
    },
};








export default function (props) {
    const useStyles = makeStyles(theme => ({
        paper: {
            padding: theme.spacing(props.padding?props.padding:10),
            width: "fit-content",
            margin: "auto",
        },
    }));
    const classes = useStyles();
    const slides = props.slides;
    const [shadow, setShadow] = useState(2);
    const mouseOver = () => {
        setShadow(20)
    };
    const mouseOut = () => {
        setShadow(2)
    };


    return (



            <Carousel
                swipeable={true}
                draggable={true}
                showDots={true}
                responsive={responsive}
                ssr={true} // means to render carousel on server-side.
                infinite={false}
                autoPlay={false}
                autoPlaySpeed={5000}
                keyBoardControl={true}

                containerClass="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={props.deviceType}
                itemClass="carousel-item-padding-40-px"
            >
                {slides.map((item)=>
                    <HoverPaper className={classes.paper}>
                        {item.element}
                    </HoverPaper>
                )}

            </Carousel>

    )
}