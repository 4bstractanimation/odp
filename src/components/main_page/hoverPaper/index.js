import React, {useState} from "react";
import {Paper} from "@material-ui/core";


export default (props) => {
    const [shadow,setShadow] = useState(2);

    function mouseOver() {
        setShadow(10)
    }
    function mouseOut() {
        setShadow(2)
    }

    return <Paper className={props.className} elevation={shadow} onMouseOut={mouseOut} onMouseOver={mouseOver} >
        {props.children}
    </Paper>
}