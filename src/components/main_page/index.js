import React, {Fragment} from "react";
import Slider from "./slider";
import Services from "./services"
import Spacing from "./spacing"
import Team from "./team"
import About from "./about"
import Packages from "./packages"
import FooterPage from "./footer";

export default () =>
    <Fragment>
        <Slider/>
        <Spacing/>
        <Services/>
        <Spacing/>
        <Team/>
        <Spacing/>
        <Packages/>
         <Spacing/>
         <About/>
         <Spacing/>
         <FooterPage/>
    </Fragment>