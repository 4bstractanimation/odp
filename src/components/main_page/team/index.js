import React, {Fragment} from "react";
import Team from "../group_slider"
import {Typography} from "@material-ui/core";





const slides = [
    { element : <Fragment>
                    <img src={require("../../../media/blackOPD.png")} style={{width:180, }} />
                    <Typography align={"center"} variant={"h6"}> Moazzam Kazmi <Typography variant={"subtitle1"}> Founder </Typography> </Typography>
                </Fragment>
    },
    { element : <Fragment>
                    <img src={"https://icon-library.net/images/profile-icon-png/profile-icon-png-16.jpg"} style={{width:180, }} />
                    <Typography align={"center"} variant={"h6"}> Zohaib Akram <Typography variant={"subtitle1"}> CEO </Typography> </Typography>
                </Fragment>
    },
    { element : <Fragment>
                    <img src={"https://icon-library.net/images/profile-icon-png/profile-icon-png-16.jpg"} style={{width:180, }} />
                    <Typography align={"center"} variant={"h6"}> Hamza Raseed <Typography variant={"subtitle1"}> Manager </Typography> </Typography>
                </Fragment>
    },
    { element : <Fragment>
                    <img src={"https://icon-library.net/images/profile-icon-png/profile-icon-png-16.jpg"} style={{width:180, }} />
                    <Typography align={"center"} variant={"h6"}> Hania Amir <Typography variant={"subtitle1"}> Director </Typography> </Typography>
                </Fragment>
    },
    { element : <Fragment>
                    <img src={"https://icon-library.net/images/profile-icon-png/profile-icon-png-16.jpg"} style={{width:180, }} />
                    <Typography align={"center"} variant={"h6"}> Fahad Khalid <Typography variant={"subtitle1"}> Ambassador </Typography> </Typography>
                </Fragment>
    },

]






export default () => <div id={"team"} style={{textAlign:"center"}}>
    <Typography align={"center"} variant={"h4"} >Our Team</Typography>
    <Team slides={slides} />
</div>