import React from "react";
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import "./style.css"
import Button from "@material-ui/core/Button";


const content = [
  {
    title: " اب  میرے بچے لیٹ نہیں ہونگے کیونکہ میرے پاس تم ہو او پی ڈی",
    description:"",
    button: "Read More",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSOVQHUPstvcD4uwvjuDhrAsdrJnpnwneMc7qg2rwTF5jLZ0kle",
  },
  {
    title: "اب مجھے کسی کا انتظار کرنے کی ضرورت نہیں ہے کیونکہ میرے پاس تم ہو او پی ڈی",
    description:"",
    button: "Discover",
    image: "https://worktalk.com/wp-content/uploads/2019/05/rushing-man.jpeg",
  },
];



export default () =>
    <div  id={"home"}>
        <Slider className="slider-wrapper slider" autoplay={3000}  >
          {content.map((item, index) => (
            <div
                className={"slider-content"}
              key={index}
              style={{ background: `url('${item.image}') no-repeat center center` }}
            >
              <div className="inner">
                <h1>{item.title}</h1>
                <p>{item.description}</p>
              </div>
            </div>
          ))}
        </Slider>
    </div>

