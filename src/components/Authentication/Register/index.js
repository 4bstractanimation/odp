
import React, {Component, Fragment, useState} from "react";
import {Paper, Grid, TextField, Button, withStyles, CircularProgress, makeStyles,} from "@material-ui/core";
import {A} from "hookrouter";
import Spacing from "../../main_page/spacing";
import app from "firebase";



const useStyles = makeStyles(theme => ({
  LoginPanel: {
      padding: 20,
      height:"fit-content",
      textAlign:"center",
  },
  root:{
      height: 624,
      marginTop:24
  },
  TextField:{
      width:100+"%",
      marginBottom:40
  },
  errorMessage:{
      color:"red",


  }
}));




function Register() {

    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [confirm_password,setConfirm_password] = useState("");
    const [error_email,setError_email] = useState("");
    const [error_password,setError_password] = useState("");
    const [error_confirm_password,setError_confirm_password] = useState("");
    const [loading, setLoading] = useState(false);



    const handleSubmit = (e ) => {
        let isValid = false;
        e.preventDefault();

        if (email===""){
            setError_email("Please enter your email");
            isValid = false
        }else {
            setError_email("");
            isValid = true
        }


        if (password===""){
            setError_password("Please enter your password");
            isValid = false
        }else {
            setError_password("");
            isValid = true
        }


        if ( password!==confirm_password){
            setError_confirm_password("Passwords does not match");
            isValid = false
        }else {
            setError_confirm_password("");
            isValid = true
        }



        if (isValid){
            register()
        }

    };

    async function register() {
        try {
              setLoading(true);
              await app.auth().createUserWithEmailAndPassword(email, password)

        }catch (e) {
                setError_email(e.message);
        }finally {
            setLoading(false)
        }
    }

    const classes = useStyles();

        return(
        <div className={classes.root}>
              <Spacing/>
            <Grid
                container
                spacing={0}

                alignItems="center"
                justify="center"
            >
                <Grid item lg={5} xl={6} xs={12} sm={11} md={8}>
                    <Paper className={classes.LoginPanel} elevation={5}>
                        <h3>Register</h3>
                            <TextField
                                error={error_email!==""}
                                className={classes.TextField}
                                label={error_email===""?"Email":error_email}
                                variant="outlined"
                                id="id_Name"
                                onChange={(e)=> {
                                        setEmail(e.target.value);
                                        setError_email("");
                                    }}
                            />

                            <TextField
                                error={error_password!==""}
                                type="password"
                                className={classes.TextField}
                                label={error_password===""?"Password":error_password}
                                variant="outlined"
                                id="id_Password"
                                onChange={(e)=> {
                                        setPassword(e.target.value);
                                        setError_password("");
                                    }}
                            />

                            <TextField
                                error={password!==confirm_password}
                                className={classes.TextField}
                                type="password"
                                label={error_confirm_password===""?"Confirm Password":error_confirm_password}
                                variant="outlined"
                                id="id_Password2"
                                onChange={(e)=> {
                                        setConfirm_password(e.target.value);
                                        setError_confirm_password("");
                                    }}
                            />

                             {loading?
                                    <CircularProgress/>
                                    :<Fragment>
                                        <Button onClick={handleSubmit} variant="contained" color="primary">
                                            Register
                                        </Button>  <Button component={A} href={"/Login"} color={"primary"} variant={"outlined"}> Login </Button>
                                    </Fragment>
                             }
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )

}

export default Register;