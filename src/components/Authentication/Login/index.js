import React, {Fragment, useState} from "react";
import {Paper, Grid, TextField, Button, CircularProgress, makeStyles} from "@material-ui/core";
import {A, navigate} from "hookrouter";
import Spacing from "../../main_page/spacing";
import {useAuthState} from "react-firebase-hooks/auth";
import app from "firebase";


const useStyles = makeStyles(theme => ({
    LoginPanel: {
        padding: 20,
        height: "fit-content",
        textAlign: "center",
    },
    root: {
        height: 624,
        marginTop: 24
    },
    TextField: {
        width: 100 + "%",
        marginBottom: 40
    },
    errorMessage: {
        color: "red",
    }
}));


function Login() {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error_email, setError_email] = useState("");
    const [error_password, setError_password] = useState("");

    const [loading, setLoading] = useState(false);
    const [user, initialising, error] = useAuthState(app.auth());




    const handleSubmit = (e) => {
        let isValid = false;

        e.preventDefault();

        if (email === "") {
            setError_email("Email required");
            isValid = false
        } else {
            setError_email("");
            isValid = true
        }


        if (password === "") {
            setError_password("Password required");
            isValid = false
        } else {
            setError_password("");
            isValid = true
        }

        if (isValid) {
            login()
        }

    };

    async function login() {
        try {
              setLoading(true);
              await app.auth().signInWithEmailAndPassword(email, password);

        }catch (e) {
                setError_email(e.message);
        }finally {
            setLoading(false)
        }
    }



    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Spacing/>
            <Grid container
                  spacing={0}
                  alignItems="center"
                  justify="center">
                <Grid item lg={5} xl={6} xs={12} sm={11} md={8}>
                    <Paper className={classes.LoginPanel} elevation={5}>
                        <h3>Login</h3>
                        <TextField

                            error={error_email!==""}
                            className={classes.TextField}
                            label={error_email===""?"Email":error_email}
                            variant="outlined"
                            id="id_Name"
                            onChange={(e) => {
                                setEmail(e.target.value);
                                setError_email("");
                            }}

                            onBlur={(e)=>{

                                    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                                    if(!re.test(e.target.value) && e.target.value !=="") {
                                        setError_email("Invalid format")
                                    }

                            }}
                        />
                        <TextField

                            error={error_password!==""}
                            className={classes.TextField}
                            label={error_password===""?"Password":error_password}
                            type="password"
                            variant="outlined"
                            id="id_Password"
                            onChange={(e) => {
                                setPassword(e.target.value);
                                setError_password("");
                            }}
                        />
                        {loading || initialising?
                            <CircularProgress/>
                            : <Fragment>
                                <Button onClick={handleSubmit} variant="contained" color="primary">
                                    Login
                                </Button>
                                <Button variant={"outlined"} component={A} href={"/Register"}
                                                     color={"primary"}> Register </Button>

                                <Button variant={"outlined"} component={A} href={"/Forgotpassword"}
                                                     color={"primary"}> Forgot Password? </Button>

                            </Fragment>
                        }
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}


export default Login
