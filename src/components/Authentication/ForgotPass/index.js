import React, {Fragment, useState} from "react";
import {Paper, Grid, TextField, Button, CircularProgress, makeStyles} from "@material-ui/core";
import {A, navigate} from "hookrouter";
import Spacing from "../../main_page/spacing";
import {useAuthState} from "react-firebase-hooks/auth";
import app from "firebase";
import {useSnackbar} from "notistack";


const useStyles = makeStyles(theme => ({
    LoginPanel: {
        padding: 20,
        height: "fit-content",
        textAlign: "center",
    },
    root: {
        height: 624,
        marginTop: 24
    },
    TextField: {
        width: 100 + "%",
        marginBottom: 40
    },
    errorMessage: {
        color: "red",
    }
}));


function ForgotPass() {

    const [email, setEmail] = useState("");
    const [error_email, setError_email] = useState("");

    const [loading, setLoading] = useState(false);
    const [user, initialising, error] = useAuthState(app.auth());

     const { enqueueSnackbar }  = useSnackbar();

    const Success = () => {
        enqueueSnackbar(
          'A reset password mail has been sent to you Email', {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };




    const handleSubmit = (e) => {
        let isValid = false;

        e.preventDefault();

        if (email === "") {
            setError_email("Email required");
            isValid = false
        } else {
            setError_email("");
            isValid = true
        }


        if (isValid) {
            resetPass().then(()=>Success());
        }

    };

    async function resetPass() {
        try {
              setLoading(true);
              await app.auth().sendPasswordResetEmail(email)

        }catch (e) {
                setError_email(e.message);
        }finally {
            setLoading(false)
        }
    }



    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Spacing/>
            <Grid container
                  spacing={0}
                  alignItems="center"
                  justify="center">
                <Grid item lg={5} xl={6} xs={12} sm={11} md={8}>
                    <Paper className={classes.LoginPanel} elevation={5}>
                        <h3>Reset Password</h3>
                        <TextField

                            error={error_email!==""}
                            className={classes.TextField}
                            label={error_email===""?"Email":error_email}
                            variant="outlined"
                            id="id_Name"
                            onChange={(e) => {
                                setEmail(e.target.value);
                                setError_email("");
                            }}
                        />
                        {loading || initialising?
                            <CircularProgress/>
                            : <Fragment>
                                <Button onClick={handleSubmit} variant="contained" color="primary">
                                    Submit
                                </Button>
                                <Button variant={"outlined"} component={A} href={"/Login"}
                                                     color={"primary"}> Login </Button>

                                <Button variant={"outlined"} component={A} href={"/Register"}
                                                     color={"primary"}> Register </Button>

                            </Fragment>
                        }
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}


export default ForgotPass;
