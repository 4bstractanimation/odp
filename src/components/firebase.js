import app from 'firebase/app'
import firebase from "firebase";



export const FireBase = () => {
    const  config = {
      apiKey: "AIzaSyCZ9010JsEE3qAP9n4ModjdZ-fRyIKcLbA",
      authDomain: "opdapp-31be3.firebaseapp.com",
      databaseURL: "https://opdapp-31be3.firebaseio.com",
      projectId: "opdapp-31be3",
      storageBucket: "opdapp-31be3.appspot.com",
      messagingSenderId: "686191285918",
      appId: "1:686191285918:web:d4e7f0cd8571c5b43ecd8f",
      measurementId: "G-8CWCMKQXWQ"
    };
    if (!firebase.apps.length) {
        app.initializeApp(config);
    }
};