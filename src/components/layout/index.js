import React from "react";
import {usePath} from "hookrouter"
import MainLayout from "./main_layout"
import LoginLayout from "./login_layout"
import UserDashboard from "./dashboard_layout"
import AdminDashboard from "./admin_dashboard"

export default  (props)=> {
    const path = usePath();
    return path==="/"?
      <MainLayout>{props.children}</MainLayout>
        :path==="/Login"|| path==="/Register" || path==="/Forgotpassword"?<LoginLayout>{props.children}</LoginLayout>
            :path.includes("/Admin")?<AdminDashboard>{props.children}</AdminDashboard>:<UserDashboard>{props.children}</UserDashboard>
}