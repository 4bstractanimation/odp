import React from 'react';
import clsx from 'clsx';
import {
    AppBar, Drawer, makeStyles, useTheme, Toolbar, List, CssBaseline, Typography
    , Divider, IconButton, ListItem, ListItemIcon, ListItemText, withWidth, CircularProgress
} from '@material-ui/core';
import {LocalTaxi, ShoppingCart , Menu , ChevronLeft , ChevronRight,  PowerSettingsNew ,Feedback} from '@material-ui/icons';
import {A, navigate} from "hookrouter";
import app from "firebase";
import {useAuthState} from "react-firebase-hooks/auth";
import Logo from "../logo";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
        height: 100,
        textTransform: "uppercase",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
      height: 100
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function NavBarDrawer(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [user, initialising, error] = useAuthState(app.auth());


  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }
  async function handleClick(){
    await app.auth().signOut();
  }
  if(!initialising) {
       if (user) {
              if (user.email !== "opdapp01@gmail.com") navigate("/Login")
       }
       if(!user) {
         navigate("/Login")
       }
  }


    return   initialising?<div style={{position:"fixed",top:"50%",left:"50%"}}><CircularProgress/></div>:(
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
          style={{alignItems:"center"}}
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}

      >
        <Toolbar>
          <IconButton
            color="inherit"

            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            style={{position:"fixed",left:25}}
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <Menu />
          </IconButton>
                  <Logo height={100} />
                  <Typography variant={"h6"}>Dashboard</Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant={props.width==="sm"|| props.width==="xs"?"temporary":"permanent"}
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}
        onClose={()=>setOpen(false)}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
          </IconButton>
        </div>
        <Divider />
        <List>

            <ListItem button component={A} href={"/Admin/Packagerequests"} >
              <ListItemIcon><ShoppingCart/> </ListItemIcon>
              <ListItemText primary={"Package Requests"} />
            </ListItem>

          <ListItem button component={A} href={"/Admin/Driverrequests"} >
              <ListItemIcon><LocalTaxi/>   </ListItemIcon>
              <ListItemText primary={"Driver Requests"} />
            </ListItem>
            <ListItem button component={A} href={"/Admin/Viewfeedback"} >
              <ListItemIcon><Feedback/>   </ListItemIcon>
              <ListItemText primary={"View Feedback"} />
            </ListItem>

            <ListItem button onClick={handleClick}>
                <ListItemIcon> <PowerSettingsNew/> </ListItemIcon>
                <ListItemText primary={"Logout"} />
            </ListItem>

        </List>

      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
          {props.children}
      </main>
    </div>
  );
}
export default withWidth()(NavBarDrawer);
