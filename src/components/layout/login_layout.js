import React, {Fragment} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {CssBaseline} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {A, navigate, usePath} from "hookrouter";
import {useAuthState} from "react-firebase-hooks/auth";
import app from "firebase";
import Logo from "../logo";





export default function ProminentAppBar(props) {

    const [user, initialising, error] = useAuthState(app.auth());

    if(!initialising) {

        if (user) {
              if (user.email === "opdapp01@gmail.com") navigate("/Admin/Packagerequests");
              else navigate('/Requestpackage');
        }
    }

  const path = usePath();

  return (
      <Fragment>
          <CssBaseline/>
          <AppBar position="fixed" color={"primary"} style={{height:100}} >
            <Toolbar>
                <Button component={A} href={"/"}  > <Logo height={100} /> </Button>

                {path==="/Login"? <Button component={A} style={{position:"fixed",right:30}} href={"/Register"} color={"inherit"}> Register </Button>
                    : <Button component={A} style={{position:"fixed",right:30}} href={"/Login"} color={"inherit"}> Login </Button>
                }
            </Toolbar>
          </AppBar>
          <div style={{marginTop:100}}/>
          {props.children}
      </Fragment>
  );
}