import React, {Fragment, useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {CssBaseline, Paper} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import IconButton from "@material-ui/core/IconButton";
import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
import {A} from "hookrouter";
import Logo from "../logo";





export default function ProminentAppBar(props) {
 const theme = useTheme();
  const [color,setColor] = useState(theme.palette.primary.main);
  const [height,setHeight] = useState(100);
  const [visible,setVisible] = useState(false);
  const [top,setTop] = useState(100);

  const toggleVisibility = () =>{
      setVisible(!visible)
  }

  const useStyles = makeStyles(theme => ({
      btn:{
          transition: "0.5s",
          height:height,
          width:200
      },
      appBar: {
          transition: "0.5s"
      },
      buttons: {
          [theme.breakpoints.down('md')]: {
              display:"none",
          },
      },
      dropDownIcon: {
          height:"fit-content",
          color:"white",
          [theme.breakpoints.up('md')]: {
              visibility:"hidden",
          },
          [theme.breakpoints.down('md')]: {
              visibility:"visible",
          },
      },
      dropDown: {
          position:"fixed",
          zIndex:99,
          width:"100%",
          visibility: visible?"visible":"hidden",
          transition: "0.5s",
          top: top,
          [theme.breakpoints.up('lg')]: {
              visibility:"hidden",
          },

      }

    }));
    const classes = useStyles();



    useEffect(() => {
        const handleScroll = () => {
          if (window.scrollY > 331) {
              setHeight(60);
              setTop(64);
          }else{
              setHeight(100);
              setTop(100);
          }
        };
        document.addEventListener('scroll', handleScroll);
        return () => {
          document.removeEventListener('scroll', handleScroll)
        }
      }, []);


  return (
      <Fragment>
          <CssBaseline/>
          <AppBar position="fixed" color={"primary"} className={classes.appBar} >
            <Toolbar>
                <Button style={{height:height}} component={A} href={"/"} >  <Logo height={height} /></Button>
                <div style={{ width:"100%", display:"flex" }}>
                    <div style={{ width:"fit-content", height:"100%", margin:"auto"}}>
                        <ButtonGroup size="large"  variant={"contained"} color={"primary"} className={classes.buttons} aria-label="small outlined button group">
                            <Button href={"#home"} className={classes.btn}>Home</Button>
                            <Button href={"#services"} className={classes.btn}>Services</Button>
                            <Button href={"#team"} className={classes.btn}>Our Team</Button>
                            <Button href={"#about"} className={classes.btn}>About Us</Button>
                        </ButtonGroup>
                    </div>
                    <IconButton aria-label="delete" className={classes.dropDownIcon} onClick={toggleVisibility}>
                      <ArrowDropDownCircleIcon fontSize="large" />
                    </IconButton>
                    <Button component={A} href={"/Login"} color={"inherit"}> Login </Button>
                </div>
            </Toolbar>
          </AppBar>

          <Paper className={classes.dropDown} elevation={10} >
              <Button href={"#home"} style={{width:"100%"}}>Home</Button>
              <Button href={"#services"} style={{width:"100%"}}>Services</Button>
              <Button href={"#team"} style={{width:"100%"}}>Our Team</Button>
              <Button href={"#about"} style={{width:"100%"}}>About Us</Button>
          </Paper>


          <div style={{marginTop:100}}/>
          {props.children}
      </Fragment>
  );
}