import React, {Fragment, useState} from "react";
import {Paper, Grid, TextField, Button, CircularProgress, makeStyles} from "@material-ui/core";
import {useAuthState} from "react-firebase-hooks/auth";
import app from "firebase";
import { useSnackbar } from 'notistack';



const useStyles = makeStyles(theme => ({
    LoginPanel: {
        padding: 20,
        height: "fit-content",
        textAlign: "center",
    },
    root: {
        height: 624,
        marginTop: 24
    },
    TextField: {
        width: 100 + "%",
        marginBottom: 40
    },
    errorMessage: {
        color: "red",
    }
}));


function RequestPackage() {

    const { enqueueSnackbar, closeSnackbar }  = useSnackbar();

    const Success = () => {
        enqueueSnackbar(
          'Feedback sent!', {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };
    const Error = (message) => {
        enqueueSnackbar(
          message, {
              variant: 'error',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };


    const [comment, setComment] = useState("");
    const [errorComment, setErrorComment] = useState("");

    const [loading, setLoading] = useState(false);
    const [user, initialising, error] = useAuthState(app.auth());



    const handleSubmit = (e) => {
        let isValid = false;

        e.preventDefault();

        if (comment === "") {
            setErrorComment("Cant send empty feedback!");
            isValid = false
        } else {
            setErrorComment("");
            isValid = true
        }


        if (isValid) {
            setLoading(true);
            const key = app.database().ref("Requests").push().getKey();

            app.database().ref("Feedback").child(key).set(
                {
                    "User" : user.uid,
                    "Comment": comment,
                    "id" : key
                }
                ).then(r => {
                    Success();
                }).catch(r=>{
                    Error(r.message);
                }).finally(()=> setLoading(false)
            )
        }
    };

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container
                  spacing={0}
                  alignItems="center"
                  justify="center">
                <Grid item lg={5} xl={6} xs={12} sm={11} md={8}>
                    <Paper className={classes.LoginPanel} elevation={5}>

                        <TextField
                            className={classes.TextField}
                            label={errorComment===""?"Enter your feedback here":errorComment}
                            error={errorComment!==""}
                            multiline
                            variant="outlined"
                            onChange={(e) => {
                                setComment(e.target.value);
                                setErrorComment("");
                            }}
                        />


                        {loading || initialising?
                            <CircularProgress/>
                            : <Fragment>
                                <Button onClick={handleSubmit} variant="contained" color="primary">
                                    Submit
                                </Button>
                              </Fragment>
                        }
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}


export default RequestPackage
