import React, {Component, forwardRef, useEffect, useState} from 'react';
import MaterialTable, {MTableCell, MTableEditField} from 'material-table';
import {
    AddBox, ArrowUpward,
    Check, ChevronLeft,
    ChevronRight,
    Clear,
    DeleteOutline,
    Edit,
    FilterList,
    FirstPage, LastPage, Remove,
    SaveAlt, Search, ViewColumn
} from "@material-ui/icons";
import {useList} from "react-firebase-hooks/database";
import app from "firebase"
import {useAuthState} from "react-firebase-hooks/auth";
import {useSnackbar} from "notistack";



function ViewExisting(){

    const [user, initialising, error] = useAuthState(app.auth());
    const { enqueueSnackbar}  = useSnackbar();

    const [data,setData] = useState();

    const Reference = app.database().ref("Requests");
    const [value_data, loading_data, error_data] = useList(Reference.orderByChild("User").equalTo(user.uid));

     const Success = (message) => {
        enqueueSnackbar(
          message, {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };

    const filter = (fil) =>{

        let index = fil.length - 1;

        while (index >= 0) {
            if(fil[index].Status === "Removed"){
                fil.splice(index, 1);
            }
            index -=1;
        }
        return fil
    }

    useEffect( () => {
        if(!loading_data){

            setData(filter(JSON.parse(JSON.stringify(value_data))));
        }
        }, [loading_data, value_data]);

    const columns = [
        { title: 'Pickup time', field: 'Pickup time' ,type:'time' , require:true},
         { title: 'Destination Address', field: 'Destination point' },
         { title: 'Pickup Address', field: 'Pickup point' },
        { title: 'Contact number', field: 'Contact number'},
         { title: 'Package', field: 'Package',
             lookup: { 1: 'Weekly', 2: 'Monthly', 3: "Yearly" },
         },
        { title: 'Price', field: 'Price', editable:"never"},
        { title: 'Status', field: 'Status', editable:"never"},
    ];




    const tableIcons = {
            Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
            Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
            Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
            Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
            DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
            Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
            Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
            FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
            LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
            NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
            PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
            ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
            Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
            SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
            ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
            ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };

        return(

            <MaterialTable
                icons={tableIcons}
                title="Existing Packages"
                columns={columns}
                data={data}
                isLoading={loading_data}
                editable={{
                    onRowDelete: oldData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                Reference.child(oldData.id).child("Status").set("Removed").then(()=>{
                                    Success("Data deleted successfully!")
                                })
                                }, 600);
                        }),
                }}

                components={{
                        Cell: props => (
                            <MTableCell data-column={props.columnDef.title} {...props} />
                        ),
                        EditField: props => (

                            <MTableEditField variant={"outlined"} label={props.columnDef.title}  {...props} />
                        ),

                    }}

                    options={{
                        columnsButton:true,
                        exportButton:true,
                    }}
            />

            )}

export default ViewExisting;
