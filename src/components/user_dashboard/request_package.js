import React, {Fragment, useState} from "react";
import {Paper, Grid, TextField, Button, CircularProgress, makeStyles, Select, MenuItem, InputLabel} from "@material-ui/core";
import Spacing from "../../components/main_page/spacing"
import {useAuthState} from "react-firebase-hooks/auth";
import app from "firebase";
import { KeyboardTimePicker,MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import FormControl from "@material-ui/core/FormControl";
import { useSnackbar } from 'notistack';
import MuiPhoneInput from "material-ui-phone-number"



const useStyles = makeStyles(theme => ({
    LoginPanel: {
        padding: 20,
        height: "fit-content",
        textAlign: "center",
    },
    root: {
        height: 624,
        marginTop: 24
    },
    TextField: {
        width: 100 + "%",
        marginBottom: 40
    },
    errorMessage: {
        color: "red",
    }
}));


function RequestPackage() {

    const { enqueueSnackbar, closeSnackbar }  = useSnackbar();

    const Success = () => {
        enqueueSnackbar(
          'Package requested successfully!', {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };
    const Error = (message) => {
        enqueueSnackbar(
          message, {
              variant: 'error',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };

    const [time,setTime] = useState(null);
    const [pickupTime, setPickupTime] = useState("");

    const [errorPickupTime, setErrorPickupTime] = useState("");

    const [pickupPoint, setPickupPoint] = useState("");
    const [errorPickupPoint, setErrorPickupPoint] = useState("");

    const [desPoint, setDesPoint] = useState("");
    const [errorDesPoint, setErrorDesPoint] = useState("");

    const [package_, setPackage] = useState("");
    const [errorPackage, setErrorPackage] = useState("");

    const [contact, setContact] = useState("");
    const [errorContact, setErrorContact] = useState("");


    const [loading, setLoading] = useState(false);
    const [user, initialising, error] = useAuthState(app.auth());



    const handleSubmit = (e) => {
        let isValid = true;

        e.preventDefault();

        if (pickupTime === "") {
            setErrorPickupTime("Please select time");
            isValid = false
        }

        if (pickupPoint === "") {
            setErrorPickupPoint("Please select pickup point");
            isValid = false
        }


        if (desPoint === "") {
            setErrorDesPoint("Please select destination point");
            isValid = false
        }
        if (package_ === "") {
            setErrorPackage("Please select a package");
            isValid = false
        }

        if (contact === "") {
            setErrorContact("Please enter contact number");
            isValid = false
        }


        if (isValid) {
            setLoading(true);
            const key = app.database().ref("Requests").push().getKey();

            app.database().ref("Requests").child(key).set(
                {
                    "User" : user.uid,
                    "Pickup time": pickupTime,
                    "Pickup point": pickupPoint,
                    "Destination point": desPoint,
                    "Package" : package_,
                    "Contact number": contact,
                    "Status" : "pending",
                    "Price":"NA",
                    "id" : key
                }
                ).then(r => {
                    Success();
                }).catch(r=>{
                    Error(r.message);
                }).finally(()=> setLoading(false)
            )
        }
    };

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Spacing/>
            <Grid container
                  spacing={0}
                  alignItems="center"
                  justify="center">
                <Grid item lg={5} xl={6} xs={12} sm={11} md={8}>
                    <Paper className={classes.LoginPanel} elevation={5}>
                        <h3>Request a package</h3>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>

                        <KeyboardTimePicker

                            style={{width:"100%", marginBottom:40}}
                            label={errorPickupTime===""?"Pickup time":errorPickupTime}
                            error={errorPickupTime!==""}
                            KeyboardButtonProps={{
                                'aria-label': 'change time',
                            }}
                            value={time}
                            onChange={(time) => {
                                if(time) {
                                    setTime(time);
                                    setPickupTime(time.toLocaleTimeString('en-US'));
                                    setErrorPickupTime("");
                                }else {
                                    setPickupTime("")
                                }

                            }}
                        />
                        </MuiPickersUtilsProvider>

                        <TextField
                            className={classes.TextField}
                            label={errorPickupPoint===""?"Pickup address":errorPickupPoint}
                            error={errorPickupPoint!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setPickupPoint(e.target.value);
                                setErrorPickupPoint("");
                            }}
                        />
                        <TextField
                            className={classes.TextField}
                            label={errorDesPoint===""?"Destination address":errorDesPoint}
                            error={errorDesPoint!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setDesPoint(e.target.value);
                                setErrorDesPoint("");
                            }}
                        />
                        <MuiPhoneInput
                            defaultCountry='pk'
                            onlyCountries={['pk']}
                            className={classes.TextField}
                            label={errorContact===""?"Contact Number":errorContact}
                            error={errorContact!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setContact(e);
                                setErrorContact("");
                            }}
                        />



                        <FormControl  variant={"outlined"} error={errorPackage!==""} className={classes.TextField}>
                            <InputLabel id="Package-Label" >{errorPackage===""?"Select Package":errorPackage}</InputLabel>

                            <Select
                              value={package_}
                              labelWidth={errorPackage===""?115:180}
                              labelId={"Package-Label"}
                              onChange={(e) => {
                                    setPackage(e.target.value);
                                    setErrorPackage("");
                                }}
                             >
                                <MenuItem value={1}>Weekly</MenuItem>
                                <MenuItem value={2}>Monthly</MenuItem>
                                <MenuItem value={3}>Yearly</MenuItem>
                            </Select>
                        </FormControl>


                        {loading || initialising?
                            <CircularProgress/>
                            : <Fragment>
                                <Button onClick={handleSubmit} variant="contained" color="primary">
                                    Submit
                                </Button>

                            </Fragment>
                        }

                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}


export default RequestPackage
