import React, {Fragment, useState} from "react";
import {Paper, Grid, TextField, Button, CircularProgress, makeStyles} from "@material-ui/core";
import {useAuthState} from "react-firebase-hooks/auth";
import app from "firebase";
import { useSnackbar } from 'notistack';
import MuiPhoneInput from "material-ui-phone-number";



const useStyles = makeStyles(theme => ({
    LoginPanel: {
        padding: 20,
        height: "fit-content",
        textAlign: "center",
    },
    root: {
        height: 624,
        marginTop: 24
    },
    TextField: {
        width: 100 + "%",
        marginBottom: 40
    },
    errorMessage: {
        color: "red",
    }
}));


function RequestPackage() {

    const { enqueueSnackbar, closeSnackbar }  = useSnackbar();

    const Success = () => {
        enqueueSnackbar(
          'Request sent!', {
              variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };
    const Error = (message) => {
        enqueueSnackbar(
          message, {
              variant: 'error',
                anchorOrigin: {
                  vertical: 'bottom',
                    horizontal: 'left',
                },
            }
        )
    };


    const [name, setName] = useState("");
    const [errorName, setErrorName] = useState("");

    const [cnic, setCnic] = useState("");
    const [errorCnic, setErrorCnic] = useState("");

    const [license, setLicense] = useState("");
    const [errorLicense, setErrorLicense] = useState("");

    const [contact, setContact] = useState("");
    const [errorContact, setErrorContact] = useState("");



    const [loading, setLoading] = useState(false);
    const [user, initialising, error] = useAuthState(app.auth());



    const handleSubmit = (e) => {
        let isValid = true;

        e.preventDefault();


        if (name === "") {
            setErrorName("Name is required");
            isValid = false
        }


        if (cnic === "") {
            setErrorCnic("Cnic is required");
            isValid = false
        }


        if (license === "") {
            setErrorLicense("license number is required");
            isValid = false
        }

        if (contact === "") {
            setErrorContact("Please enter contact number");
            isValid = false
        }


        if (isValid) {
            setLoading(true);
            const key = app.database().ref("Requests").push().getKey();

            app.database().ref("Drivers").child(key).set(
                {
                    "User" : user.uid,
                    "Name": name,
                    "Cnic": cnic,
                    "License number": license,
                    "Status" : "pending",
                    "Contact number": contact,
                    "id" : key
                }
                ).then(r => {
                    Success();
                }).catch(r=>{
                    Error(r.message);
                }).finally(()=> setLoading(false)
            )
        }
    };

    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container
                  spacing={0}
                  alignItems="center"
                  justify="center">
                <Grid item lg={5} xl={6} xs={12} sm={11} md={8}>
                    <Paper className={classes.LoginPanel} elevation={5}>

                        <TextField
                            className={classes.TextField}
                            label={errorName===""?"Name":errorName}
                            error={errorName!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setName(e.target.value);
                                setErrorName("");
                            }}
                        />
                        <TextField
                            className={classes.TextField}
                            label={errorCnic===""?"Cnic number":errorCnic}
                            error={errorCnic!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setCnic(e.target.value);
                                setErrorCnic("");
                            }}
                        />

                         <TextField
                            className={classes.TextField}
                            label={errorLicense===""?"License number":errorLicense}
                            error={errorLicense!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setLicense(e.target.value);
                                setErrorLicense("");
                            }}
                        />

                        <MuiPhoneInput
                            defaultCountry='pk'
                            onlyCountries={['pk']}
                            className={classes.TextField}
                            label={errorContact===""?"Contact Number":errorContact}
                            error={errorContact!==""}
                            variant="outlined"
                            onChange={(e) => {
                                setContact(e);
                                setErrorContact("");
                            }}
                        />


                        {loading || initialising?
                            <CircularProgress/>
                            : <Fragment>
                                <Button onClick={handleSubmit} variant="contained" color="primary">
                                    Submit
                                </Button>
                              </Fragment>
                        }
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}


export default RequestPackage
