import React, {Fragment} from "react";
import {Paper, Typography} from "@material-ui/core";
import Animation from "../../Animations"
import Grid from "@material-ui/core/Grid";

export default ()=>
    <Fragment>
    <Grid container justify={"center"} style={{textAlign:"center"}}>
        <Grid xs={12} sm={10} md={8} lg={6}>
            <Paper style={{padding:50}}>
                <Typography variant={"h5"}>Congratulations your request is accepted!</Typography>
                <Animation height={500} width={"100%"} loop={false} />
            </Paper>
        </Grid>
    </Grid>
    </Fragment>