import React, {Fragment, useEffect, useState} from "react";
import BecomeDriver from "./become_driver"
import app from "firebase";
import {useList} from "react-firebase-hooks/database";
import {useAuthState} from "react-firebase-hooks/auth";
import Accepted from "./accepted"
import {CircularProgress, Typography} from "@material-ui/core";
import Pending from "./pending"
import Rejected from "./rejected"
import Spacing from "../../main_page/spacing";

export default ()=>{

    const [user, initialising, error] = useAuthState(app.auth());
    const Reference = app.database().ref("Drivers");
    const [value_data, loading_data, error_data] = useList(Reference.orderByChild("User").equalTo(user.uid));
    const [data,setData] = useState();

    useEffect( () => {

        async function exe() {

            if(loading_data){
                setData(0)
            }else{
                if(value_data.length!==0 ) {
                    value_data.forEach((item) => {
                        const stat = item.val().Status;
                        stat === "pending" ? setData(1)
                            : stat === "accepted" ? setData(2)
                            : stat === "rejected" ? setData(3)
                                : setData(4)
                    })
                }else {
                    setData(4)
                }
            }

        }

        exe();

        }, [loading_data, value_data]);


    return(
        <div style={{textAlign:"center"}}>
            <Spacing/>
            <Typography variant={"h4"}>Become a driver</Typography>
            {data===0?<CircularProgress style={{position:"absolute",left:"50%",top:"50%"}} />
            :data===1?<Pending/>
            :data===2?<Accepted/>
            :data===3?<Rejected/>
            :<BecomeDriver/>}
        </div>
    )

};