import React from 'react';
import Lottie from 'lottie-react-web'
import Rejected from './Rejected'
import Accepted from "./Accepted"
import Pending from "./Pending"

export default function (props) {

    let animation = Accepted;
    if(props.animation==="pending"){
         animation  = Pending
    }else if(props.animation==="rejected"){
        animation = Rejected
    }

    return (
        <Lottie
            speed={1}
            height={props.height ? props.height : 230}
            width={props.width ? props.width : 230}
            options={{

                animationData: animation,
                loop: props.loop,
            }}

        />
    )
}

