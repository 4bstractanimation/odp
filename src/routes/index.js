import React from "react";
import MainPage from "../components/main_page"
import {useRoutes} from "hookrouter";
import Layout from "../components/layout"
import Login from "../components/Authentication/Login";
import Register from "../components/Authentication/Register";
import ForgotPassword from "../components/Authentication/ForgotPass"
import {RequestPackage,ViewExisting,BecomeDriver,Feedback} from "../components/user_dashboard"
import {PackageRequests,DriverRequests,ViewFeedback} from "../components/admin_dashboard"



const routes = {
    "/" : () => <MainPage/>,
    "/Login" :()=><Login/>,
    "/Forgotpassword" :()=><ForgotPassword/>,
    "/Register" :()=><Register/>,
    "/Requestpackage" :()=><RequestPackage/>,
    "/Viewexisting" :()=><ViewExisting/>,
    "/Becomedriver" :()=><BecomeDriver/>,
    "/Feedback" :()=><Feedback/>,
    "/Admin/Packagerequests" :()=><PackageRequests/>,
    "/Admin/Driverrequests" :()=><DriverRequests/>,
    "/Admin/Viewfeedback" :()=><ViewFeedback/>,
};

function Route() {
  const routeResult = useRoutes(routes);
  return(
      <Layout>
          {routeResult}
      </Layout>
  )
}

export default Route;

